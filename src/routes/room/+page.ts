import { redirect } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = ({ url }) => {
	const id = url.searchParams.get('id');
	if (!id) throw redirect(302, '/');
	return { id };
};
