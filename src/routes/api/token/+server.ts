import { env } from '$env/dynamic/private';
import { json } from '@sveltejs/kit';
import { AccessToken } from 'livekit-server-sdk';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request }) => {
	const body = await request.clone().json();

	if (typeof body.username !== 'string') {
		return json({ message: 'username is required' }, { status: 400 });
	}
	if (typeof body.room !== 'string') {
		return json({ message: 'room is required' }, { status: 400 });
	}

	const identity = crypto.randomUUID();
	const token = new AccessToken(env.LIVEKIT_API_KEY, env.LIVEKIT_SECRET_KEY, {
		identity,
		name: body.username
	});
	token.addGrant({
		roomJoin: true,
		room: body.room,
		canPublish: true,
		canSubscribe: true,
		canPublishData: true
	});

	return json({ token: token.toJwt(), identity });
};
