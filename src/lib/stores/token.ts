import { writable } from 'svelte/store';

export const token = writable<{ token: string; identity: string; username: string } | null>(null);
