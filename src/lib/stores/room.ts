import { PUBLIC_LIVEKIT_URL } from '$env/static/public';
import { ParticipantEvent, Room, RoomEvent } from 'livekit-client';
import { get, writable } from 'svelte/store';
import { token } from './token';

export const room = writable<Room | null>(null);

export async function connectToRoom(token: string, onDisconnect: () => void) {
	const r = new Room({
		adaptiveStream: true,
		dynacast: true,
		publishDefaults: {
			simulcast: true,
			stopMicTrackOnMute: true
		}
	});

	const startTime = Date.now();
	// r.prepareConnection(PUBLIC_LIVEKIT_URL);
	// console.log(`prepareConnection took ${Date.now() - startTime}ms`);

	r.on(RoomEvent.ParticipantConnected, (p) => {
		console.log('Participant connected', p.identity, p.name, p.metadata);
		room.set(r);
		// participants.update((ps) => [...ps, p]);
		p.on(ParticipantEvent.TrackMuted, (pub) => {
			console.log(p.name, 'muted', pub.trackSid);
			// participants.update((ps) => ps.map((x) => (x.identity === p.identity ? p : x)));
			room.set(r);
		})
			.on(ParticipantEvent.TrackUnmuted, (pub) => {
				console.log(p.name, 'unmuted', pub.trackSid);
				// participants.update((ps) => ps.map((x) => (x.identity === p.identity ? p : x)));
				room.set(r);
			})
			.on(ParticipantEvent.ConnectionQualityChanged, (q) => {
				console.log(p.name, 'changed connection quality', q);
				// participants.update((ps) => ps.map((x) => (x.identity === p.identity ? p : x)));
				room.set(r);
			})
			.on(ParticipantEvent.IsSpeakingChanged, (s) => {
				console.log(p.name, 'changed speaking state', s);
				// participants.update((ps) => ps.map((x) => (x.identity === p.identity ? p : x)));
				room.set(r);
			});
	})
		.on(RoomEvent.ParticipantDisconnected, (p) => {
			console.log('Participant connected', p.identity, p.name, p.metadata);
			// participants.update((ps) => {
			// 	console.log(ps);
			// 	return ps.filter((x) => x.identity !== p.identity);
			// });
			room.set(r);
		})
		.on(RoomEvent.DataReceived, (rawData, p) => {
			const data = new TextDecoder().decode(rawData);
			console.log('Received data', data, p ? 'from ' + p.identity : '');
			room.set(r);
		})
		.on(RoomEvent.Disconnected, (reason) => {
			console.log('Disconnected', reason);
			// participants.set([]);
			onDisconnect();
			room.set(null);
		})
		.on(RoomEvent.Reconnecting, () => {
			console.log('Reconnecting to room');
			room.set(r);
		})
		.on(RoomEvent.Reconnected, () => {
			room.set(r);
			console.log('Successfully reconnected. server', r.engine.connectedServerAddress);
		})
		.on(RoomEvent.LocalTrackPublished, (s) => {
			room.set(r);
			console.log('Local track published', s);
			// localParticipant.set(p);
		})
		.on(RoomEvent.LocalTrackUnpublished, (s) => {
			console.log('Local track unpublished', s);
			room.set(r);
			// localParticipant.set(p);
		})
		.on(RoomEvent.RoomMetadataChanged, (metadata) => {
			console.log('New metadata for room', metadata);
			room.set(r);
		})
		.on(RoomEvent.AudioPlaybackStatusChanged, () => {
			console.log('Audio playback status changed', r.canPlaybackAudio);
			room.set(r);
		})
		.on(RoomEvent.TrackSubscribed, (_t, s, p) => {
			console.log('subscribed to track', s.trackSid, p.identity);
			room.set(r);
			// participants.update((ps) => ps.map((x) => (x.identity === p.identity ? p : x)));
		})
		.on(RoomEvent.TrackUnsubscribed, (_t, s, p) => {
			console.log('unsubscribed from track', s.trackSid, p.identity);
			room.set(r);
			// participants.update((ps) => ps.map((x) => (x.identity === p.identity ? p : x)));
		})
		.on(RoomEvent.SignalConnected, async () => {
			const signalConnectionTime = Date.now() - startTime;
			console.log(`signal connection established in ${signalConnectionTime}ms`);
			room.set(r);
		});

	try {
		await r.connect(PUBLIC_LIVEKIT_URL, token, { autoSubscribe: true });
		room.set(r);
		// localParticipant.set(r.localParticipant);
		// participants.set([...r.participants.values()]);
		const elapsed = Date.now() - startTime;
		console.log(
			`successfully connected to ${r.name} in ${Math.round(elapsed)}ms`,
			r.engine.connectedServerAddress
		);
	} catch (error) {
		const message = (error as Error).message || 'unknown error';
		console.log('could not connect:', message);
		return;
	}
}

export function leaveRoom() {
	const r = get(room);

	if (!r) return;

	if (r.localParticipant.isCameraEnabled) {
		r.localParticipant.setCameraEnabled(false);
	}
	if (r.localParticipant.isMicrophoneEnabled) {
		r.localParticipant.setMicrophoneEnabled(false);
	}

	r.disconnect();
	room.set(null);
	token.set(null);
}
