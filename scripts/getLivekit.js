import { spawn } from 'child_process';
import fs from 'fs';
import { createInterface } from 'readline';

const readline = createInterface({
	input: process.stdin,
	output: process.stdout
});

const os = process.platform.replace('win32', 'windows').replace('darwin', 'macos');

switch (os) {
	case 'windows':
		if (!fs.existsSync('./livekit-server.exe')) {
			downloadForWindows();
		}
		break;
	case 'macos':
		readline.question('Install livekit using Homebrew? (y/n)', (answer) => {
			if (answer.toLowerCase() === 'y') downloadForMacOS();
			console.log('Not installing livekit');
		});
		break;
	case 'linux':
		if (!fs.existsSync('./livekit-server')) {
			downloadForLinux();
		}
		break;
	default:
		console.error('Unsupported OS. Please install livekit manually.');
		process.exit(1);
}

function downloadForWindows() {
	const child = spawn('powershell', [
		'Invoke-WebRequest',
		'-Uri',
		'https://github.com/livekit/livekit/releases/latest/download/livekit_1.3.1_windows_amd64.zip',
		'-OutFile',
		'livekit.zip'
	]);
	if (child.stdout) {
		child.stdout.on('data', (data) => {
			console.log(data.toString());
		});
	}
	if (child.stderr) {
		child.stderr.on('data', (data) => {
			console.error(data.toString());
		});
	}
	child.on('exit', () => {
		const child = spawn('powershell', ['Expand-Archive', 'livekit.zip', '-DestinationPath', '.']);
		if (child.stdout) {
			child.stdout.on('data', (data) => {
				console.log(data.toString());
			});
		}
		if (child.stderr) {
			child.stderr.on('data', (data) => {
				console.error(data.toString());
			});
		}
		child.on('exit', () => {
			console.log('livekit-server.exe downloaded');
		});
	});
}

function downloadForMacOS() {
	const child = spawn('brew', ['install', 'livekit']);
	if (child.stdout) {
		child.stdout.on('data', (data) => {
			console.log(data.toString());
		});
	}
	if (child.stderr) {
		child.stderr.on('data', (data) => {
			console.error(data.toString());
		});
	}

	child.on('close', (code) => {
		console.log(`Livekit installed with exit code ${code}`);
	});
}

function downloadForLinux() {
	const child = spawn('wget', ['https://get.livekit.io', '|', 'bash']);
	if (child.stdout) {
		child.stdout.on('data', (data) => {
			console.log(data.toString());
		});
	}
	if (child.stderr) {
		child.stderr.on('data', (data) => {
			console.error(data.toString());
		});
	}

	child.on('close', (code) => {
		console.log(`Livekit installed with exit code ${code}`);
	});
}
