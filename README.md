# LiveKit Demo

Realtime group WebRTC Meeting rooms built with LiveKit and SvelteKit.

## Get started

1. Clone this repository and install dependencies using your preferred node package manager. I'll be using `pnpm` in this example.

```bash
git clone https://gitlab.com/arnu515-projects/livekit-demo livekit-demo
cd livekit-demo
pnpm install
```

2. [Install Livekit](https://docs.livekit.io/getting-started/server-setup/) using that link, or run `node scripts/getLivekit.js` to download it. You can also use `pnpm run livekit` to create a LiveKit docker container instead.

3. Create a file called `.env` and put this in it:

```
LIVEKIT_API_KEY=devkey
LIVEKIT_SECRET_KEY=secret
PUBLIC_LIVEKIT_URL=http://localhost:7880
```

Remember to change these values in production.

4. Start the server with `pnpm run dev` and enjoy!
